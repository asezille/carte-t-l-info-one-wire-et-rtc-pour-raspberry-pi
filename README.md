# Documentation pour une carte d'interface spécifique TELEINFO, ONEWIRE ET RTC DS 1302 pour la Raspberry Pi #

Cette carte s'installe sur le port GPIO P1 de la Raspberry Pi.

---

* Attention la liaison téléinfo est fournie par votre compteur EDF, **vous risquez l'électrocution!**
* ** *DANGER DE MORT!* **
* **COUPEZ LE COURANT AU *DISJONCTEUR PRINCIPAL* AVANT TOUTE INTERVENTION**.
* L'ENSEMBLE DU MATERIEL DOIT ETRE CONTENU DANS UNE BOITE ISOLANTE.
* VOUS DEVEZ ETRE COMPETENT ET QUALIFIE.
* PROTEGEZ TOUT ACCES A L'ENSEMBLE DES ELEMENTS.
* VOUS ETES SEUL RESPONSABLE DES CONSEQUENCES DE CES BRANCHEMENTS. 

---

Elle permet de connecter 3 péripéhriques particuliers:

* un compteur électronique EDF selon la norme Teleinfo 
* un ou plusieurs capteurs de température conformes aux normes One Wire en 3,3 Volts comme les DS18B20
* une carte spécifique RTC DS 1302 disponible sur ebay et d'autres sites

![vuetitre.jpg](https://bitbucket.org/repo/M7zqM8/images/2407242505-vuetitre.jpg)
***

[TOC]

***

## La définition technique de la carte ##

### Schéma ###

Le schéma est très simple et il suffit de voir l'image ci-dessous avec en jaune les piste face supérieure, en orange les pistes face inférieure

![SingleTeleinfoSingleOneWire Rpi_circuit imprimé.png](https://bitbucket.org/repo/M7zqM8/images/1445593394-SingleTeleinfoSingleOneWire%20Rpi_circuit%20imprim%C3%A9.png)

La carte se connecte sur le port P1 de la Raspberry Pi

![Définition du port P1 de la Raspberry Pi](http://elinux.org/images/2/2a/GPIOs.png)


### BOM ###

Elle est assez courte:

![IMAG1694.jpg](https://bitbucket.org/repo/M7zqM8/images/2065950573-IMAG1694.jpg)

Nb les liens ci-dessous sont des exemples des composants pris en compte, il est possible de trouver ces composants ou leurs équivallent chez bien d'autres fournisseurs.

Les frais de port et quantités minimales d'achats sont très significatifs sur le prix de revient.

Chez Fritzing le CI nu revient a plus de 18,05 € par pièce = 4 € de dossier + 9,9 € de frais de port + 6,72 € par CI fabriqué. 
Par 3, c'est 34,05 € soit encore 11,35 € pièce...

Repère | Coté de montage sur la carte | Composant
:------| :---------------------------:|:---------
CI | Circuit imprimé, repères des composants sur le dessus | Circuit imprimé à fabriquer à l'aide des fichiers gerber de ce dépôt
R1 | Dessus dressé corps à la verticale | [Résistance 1/4w 5 Kohms](http://www.rs-particuliers.com/WebCatalog/Resistance_au_carbone__0_25%C2%A0W__5%C2%A0__91R-7077583.aspx)
R2 | Dessus à l'horizontale  | [Résistance 1/4w 5 Kohms](http://www.rs-particuliers.com/WebCatalog/Resistance_au_carbone__0_25%C2%A0W__5%C2%A0__91R-7077583.aspx)
U1 | dessus, polarisé,pin vers le bornier à vis | [Optocoupleur DIL SFH620A-3 ](http://www.rs-particuliers.com/WebCatalog/Optocoupleur_Traversant__Vishay__SFH620A_3__sortie_Transistor-6998253.aspx)
Rpi P1 | DESSOUS | [connecteur femelle 2x13 pins au pas de 2,54 mm](http://www.rs-particuliers.com/WebCatalog/Support_pour_circuit_imprime__26_Contacts__2_Rangees__Traversant__Droit__au_pas_de_254mm-6811351.aspx)
connecteur vis | Dessus, arrivée des fils par le bord extérieur de la carte | [Bornier à vis 5 points au pas de 5 mm apte au 230 V](http://www.rs-particuliers.com/WebCatalog/BORNIER_CI_5MM___5CTS-3617695.aspx)
RTC | dessus, la carte DS1302 se monte tête en bas | [connecteur 5 points pas de 2,54 mm](http://www.rs-particuliers.com/WebCatalog/Support_pour_circuit_imprime__5_Contacts__1_rangee__Traversant__Droit__au_pas_de_254mm-6816823.aspx)

La carte DS1302 est disponible, encore une fois par exemple, ebay, hobbytronics et d'autres surement.

Les thermomètres DS18B20 One Wire sont aussi disponible chez bien des fournisseurs ebay encore mais pas seulement.

### Fichiers de définition ###

Voyez le dépôt pour les fichiers nommés en * _ etch _ *.pdf

### Montage ###

Il suffit de souder les composants traversants mentionnés sur la BOM:

1. Les photographies ci-jointes montrent le résultat,
1. le connecteur P1 du Raspberry Pi étant sous la carte, le connecteur femelle du CI doit être soudé au-dessus
1. L'optocoupleur est polarisé, il faut impérativement le monter dans le "bon" sens: son boitier a une marque en "créneau" du coté de la pin 1 et doit être monté vers le bornier à vis
1. La carte DS1302 est montée "tête en bas" afin de conserver un CI de taille minimaliste et des distances entre cartes convenables
1. D'évidence une entretoise de 25 mm de long se fixant dans la carte DS1302 sera un plus

Vue coté bornier à vis:

![IMAG1687.jpg](https://bitbucket.org/repo/M7zqM8/images/1052961314-IMAG1687.jpg)

Vue coté alimentation Raspberry Pi

![IMAG1688.jpg](https://bitbucket.org/repo/M7zqM8/images/1306802527-IMAG1688.jpg)

### Installation ###

La Raspberry Pi devra être installée à coté du compteur EDF, alimentée en 5V et dans une boite protectrice convenable au contexte: **Attention aux poussières, à l'humidité et la température.**

Vous allez raccorder la carte téléinfo au compteur EDF, à un ou plusieurs thermomètres One Wire et à une carte DS 1302.

**Attention aux risques d'électrocution, *coupez le courant ** *AU DISJONCTEUR PRINCIPAL DE L'INSTALLATION* ** avant toute intervention*:**

* Vous risquez de vous tromper et de brancher la carte sur le 230V, 
* le compteur électrique peut être défectueux et fournir du 230V *même sur la sortie téléinfo*.
* utilisez du fil téléreport conçu pour cet usage
* Remettez les capots de protection en place après cablage sur le compteur EDF
* L'ensemble de la RASPBERRY PI et des accesoires doit être placé dans une boite isolante électriquement, verrouillées et à l'abris des poussières, humidité et plus généralement de toute intrusion entrainant des risques électriques.
* VOUS ETES SEUL RESPONSABLE DES BRANCHEMENTS ET PRECAUTIONS A PRENDRE POUR ASSURER VOTRE SECURITE ET CELLE DES TIERS
* N'INTERVENEZ QUE SI VOUS ETES COMPETENT ET QUALIFIE 
* LE 230V TUE


#### Raccordement de bus téléinfo

Le bus téléinfo est spécifique aux compteurs électroniques EDF.
Il utilise 2 fils repérés I1 et I2 sur le compteur et sur le bornier à vis, il n'y a pas de polarité, vous pouvez échanger I1 et I2 entre le compteur et la carte.
Utilisez un câble téléreport adapté aux tensions et courants de ce bus.

Borne du borniers à vis 5 points | connecteur à vis du compteur EDF
:-----------------------:| :---------------------------------:
I1 | I1
I2 | I2

----
Vues coté compteur EDF:
![IMAG1662.jpg](https://bitbucket.org/repo/M7zqM8/images/3035842010-IMAG1662.jpg)


![IMAG1663.jpg](https://bitbucket.org/repo/M7zqM8/images/1277098062-IMAG1663.jpg)

Vue coté interface:

![IMAG1661.jpg](https://bitbucket.org/repo/M7zqM8/images/444226658-IMAG1661.jpg)
----

#### Raccordement de thermomètres OneWire DS18B20

Ces thermomètres utilisent une liaison à 3 fils dont un seul sert pour les données, les 2 autres sont la masse (GND) et l'alimentation +3,3V

Borne du borniers à vis 5 points | couleur usuelle du cable du DS18B20
:-----------------------| :---------------------------------:
GND (milieu) | noir généralement repéré Gnd
Data | bleu généralement repéré Data
+3,3V | rouge généralement repéré +Vcc

Disponibles un peu partout, prenez une version étanche sans connecteur, il en existe avec des cables de 1m, 2m, 3m, 5m et 10m.
Le module du kernel linux détecte automatiquement les thermomètres branchés et crée des pseudo fichiers dans /dev...

#### Raccordement de la carte DS1302

Elle est montée "tête en bas" comme sur cette photographie:

![IMAG1687.jpg](https://bitbucket.org/repo/M7zqM8/images/3572659989-IMAG1687.jpg)

La pile sera bonne à changer un jour, mais votre Raspberry Pi le sera probablement aussi à ce moment là...

***

## Les limitations électriques ##

***

### Le bus One Wire ###

1. périphériques 3,3V exclusivement (DS18B20)
1. total du courant consommé: 30 mA maximum, c'est la principale limite liée à la Raspberry Pi
1. pull-up 5Kohms sur le fil data du One Wire permettant un fonctionnement théorique sans alimentation +3,3V. Il n'est pas certain que le module kernel accepte ce mode de fonctionnement.

### Le bus téléinfo ###

Un seul bus disponible connecté sur l'unique UART de la Raspberry Pi au travers d'un optocoupleur.
En théorie, le risque en cas de branchement sur le 230V n'est pas pour la carte, ni la Raspberry Pi mais bien pour vous: Soyez très prudent, coupez le courant avant d'intervenir même si tout semble fonctionner normalement.

### La carte horloge RTC DS1302 ###

N'utilisez qu'une version 3,3v ayant exactement le brochages décrit ici [lien vers le site Hobbytronic DS1302](http://www.hobbytronics.co.uk/real-time-clock-module-ds1302), également disponible sur d'autres sites comme ebay.

![IMAG1685.jpg](https://bitbucket.org/repo/M7zqM8/images/1101948598-IMAG1685.jpg)

***

## Les logiciels d'exploitation ##

***

Cette carte utilise les ports usuels pour les différentes interfaces sur une RAspberry Pi:
* UART RX pour la liaison téléinfo
* GPIO P1-07 pour le bus One Wire (data)
* GPIO P1-17, P1-18 et P1-27 pour la carte RTC DS 1302
* 3,3v et GND

En principe cette configuration matérielle correspond à la configuration par défaut du Kernel tant pour les numéros des GPIO que pour les pull-up pull-down configurés au boot.

### Le bus One Wire et le bus téléinfo ###

Voyez le dépôt pour le script de configuration judicieusement nommé setup_teleinfo.sh ainsi que les scripts d'exploitations nommés w1Sender.py et teleinfoSender.py.

La liaison téléinfo arrive sur le port série /dev/ttyAMA0 à 1200 bauds. Vous ne pouvez avoir qu'un seul script à la fois lisant ce port...

Les thermomètres one wire sont visibles dans le répertoire /sys/bus/w1/devices et "sudo cat /sys/devices/w1_bus_master1/w1_master_slaves" vous donnera une liste des thermomètres détectés.

Vous pouvez utiliser tous les logiciels et drivers utilisant les ports GPIO ci-dessus et disponibles sur:
* [code.google.com teleinfuse](https://code.google.com/p/teleinfuse/) en prenant soin de changer le nom du fichier du port série (la version livrée est destinée à un PC)
* [code.google.com teleinfofs](https://code.google.com/p/teleinfofs/) idem teleinfuse
* [github](https://github.com/search?q=teleinfo&ref=cmdform) par exemple,
* ou sur [raspberrypi.org](http://www.raspberrypi.org/forums/search.php?keywords=teleinfo&sid=203865bc7481cbd209cbe0c4982cd537)

Les scripts python sont des exemples très simple qui envoient les mesures dans des fichiers standards sur le ram disque /tmp.
Evitez l'enregistrement direct sur la carte SD car elle ne tiendra pas longtemps: il y a typiquement 3000 trames téléinfo par heure!

Pour sauvegarder ces fichiers toutes les 4/5 heures sur une carte SD et démarrer le tout, utilisez une tache cron à paramétrer par 
```
#!bash
$sudo crontab -e

```
puis renseigner en bas de l'écran une ligne comme ici
```
#!cron

# Edit this file to introduce tasks to be run by cron.
#
# Each task to run has to be defined through a single line
# indicating with different fields when the task will be run
# and what command to run for the task
#
# To define the time you can provide concrete values for
# minute (m), hour (h), day of month (dom), month (mon),
# and day of week (dow) or use '*' in these fields (for 'any').#
# Notice that tasks will be started based on the cron's system
# daemon's notion of time and timezones.
#
# Output of the crontab jobs (including errors) is sent through
# email to the user the crontab file belongs to (unless redirected).
#
# For example, you can run a backup of all your user accounts
# at 5 a.m every week with:
# 0 5 * * 1 tar -zcf /var/backups/home.tgz /home/
#
# For more information see the manual pages of crontab(5) and cron(8)
#
# m h  dom mon dow   command
1 */6 * * * cp -Ruvp /tmp/*.log /var/log/
# démarrage des scripts au boot système avec screen pour avoir une console détachée du terminal courant
# ces 2 lignes sont inutiles si vous utilisez setup_teleinfo.sh qui installe l'équivallent comme un service système
@reboot  screen -d -m -S thermo python /opt/teleinfo/w1Sender.py
@reboot  screen -d -m -S teleinfo python /opt/teleinfo/teleinfoSender.py


```


Vous aurez ainsi une espérance de vie de votre carte SD de 4 ou 5 ans et un risque de perte des données de 6 heures en cas de coupure de courant.
Attention à la taille des fichiers, sur quelques jours pas de problèmes, mais sur l'année il vaudra mieux horodater les fichiers avec la date de sauvegarde sur la carte sd et les effacer de /tmp. Ils seront recréés automatiquement sur /tmp.

### La carte horloge RTC DS1302 ###

Cette carte est assez spécifique pour sa liaison série, du coup il n'y a pas de module pour le kernel et il faut passer par un logiciel ad'hoc...

Voyez le dépôt [RTC DS 1302](https://asezille@bitbucket.org/asezille/raspberry-pi-rtc-ds1302.git)