#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import glob
import time
import argparse

base_dir = '/sys/bus/w1/devices'

def prepare_onewire():
    print "Démarrage de l'inventaire des thermomètres one wire"
    os.system('sudo modprobe w1-gpio')
    os.system('sudo modprobe w1-therm')  
    f = open(base_dir + '/w1_bus_master1/w1_master_slave_count', 'r');
    sensorCount = f.readlines()
    sensorCount = [int(l[0]) for l in sensorCount]
    f.close()
    print '%s thermomètres détectés' % str(sensorCount)
    f = open(base_dir + '/w1_bus_master1/w1_master_slaves', 'r');
    devices = f.readlines()
    f.close()
    return sensorCount, devices

def read_temp_raw(sensor):
    device_file = base_dir + '/' + sensor + '/w1_slave'
    f = open(device_file, 'r')
    lines = f.readlines()
    f.close()
    return lines

def read_temp(sensor):
    lines = read_temp_raw(sensor)
    while lines[0].strip()[-3:] != 'YES':
        time.sleep(0.2)
        lines = read_temp_raw(sensor)
    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos+2:]
        return temp_string[:-1]
    else:
        print "Pas de t= dans la ligne '%s'" % lines[1]
        return None

def Boucle_infinie(nomfichier, duree=600):
    sensorCount, devices = prepare_onewire()
    if sensorCount>0:
        while True:
          for s in devices:
            sensor=s[:-1]
            temp_lue=read_temp(sensor)
            if temp_lue is None:
              print 'Pas de température lue sur le thermomètre %s' % sensor
            else:
              nomf=nomfichier+"_"+str(sensor)+".log"
              with open(nomf,'a') as f:
                f.write(str(time.time())+","+str(temp_lue)+"\n") # makes it a csv
          time.sleep(duree)
    else:
         print "Aucun de thermomètre détecté sur le bus one wire..."

#------------------------------------------------------------------------------
# MAIN
#------------------------------------------------------------------------------
def main():
  parser = argparse.ArgumentParser(description='Stockage des mesures de températures provenant du bus onewire.')
  parser.add_argument('-d', '--duree',help='%(prog)s stockera les mesures prises toutes les n secondes (défaut 600 secondes)', type=int,
                      default=600)
  parser.add_argument('-l', '--log',help='%(prog)s stockera les données dans les fichiers dont la racine est fournie en option (défaut /tmp/onewire_nomthermo.log)',
                      default='/tmp/onewire')
  ar=parser.parse_args()
  nomfichier=ar.log
  duree=ar.duree
  print 'Prêt à envoyer les températures dans le fichier %s' % nomfichier
  print 'Une mesure par thermomètre toutes les %s secondes' % duree
  Boucle_infinie(nomfichier, duree)


if __name__ == "__main__":
  main()
