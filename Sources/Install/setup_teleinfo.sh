#!/bin/bash
# installation des applications de teleinfo et onewire pour envoi sur emoncms

#variables globales
heuredebut=`date +%s`
titreprecedent=""

is_file_exits(){
	local f="$1"
	[[ -f "$f" ]] && return 0 || return 1
}


titre() {
	  # affiche un titre correctement présenté
	  local t="$1"
	  local c="$2"
          if [ $titreprecedent != "" ]
          then
		  echo -e "\n---------------------------------------------------"
		  echo 'Fin de $titreprecedent'
		  echo -e "---------------------------------------------------\n\n\n"
          fi
	  echo -e "\n+++++++++++++++++++++++++++++++++++++++++++++++++++"
	  echo 'Début de $t'
	  echo -e "+++++++++++++++++++++++++++++++++++++++++++++++++++"
	  if [ "$c" != "" ] 
	  then
	    echo -e "$c"
	  else
	    echo -e "\n\n"
	  fi
	  titreprecedent="$t"
}

deplacer() {
	  local fs="$1"
	  local fd="$2"
	  if ( is_file_exits "$fs" )
	  then
	    echo "Copie du fichier $fs vers $fd"
	    sudo cp  "$fs" "$fd" -n 
	    echo "Suppression du fichier $fs"
	    sudo rm "$fs"
	  else
	    echo "Fichier $fs non trouvé, aucune action réalisée"
	  fi
}

systeme() {
	titre "la mise à jour Raspbian.." "Installation de Git et autre utilitaires indispensable.."
	sudo apt-get update -y
	sudo apt-get install -y git git-core screen 
	sudo apt-get install -y build-essential python python-dev 
	sudo apt-get install -y python-pkg-resources python-setuptools
	# apt-get installs outdated python 2.6 which is not advisable as we are in 2.7
	sudo wget https://bootstrap.pypa.io/get-pip.py
	sudo python get-pip.py
	sudo pip install pyserial
}

teleinfo() {
  titre "la copie des fichiers programmes et préparation des démons lancés automatiquement au boot" "\n\n"
	sudo chmod +x '../*.py'
	sudo chmod +x '../*.sh'
	sudo chmod +x './suiviteleinfo'
  sudo mkdir /opt/teleinfo
  sudo cp '../*.py' /opt/teleinfo/
  sudo cp '../*.sh' /opt/teleinfo/
  sudo cp './suiviteleinfo' /etc/init.d/
	echo "sudo screen -list pour lister les démons en cours d'utilisation"
 	sudo update-rc.d suiviteleinfo defaults
}

uart() {
	titre " la préparation de la Raspberry Pi pour l'utilisation de l'uart" "L'uart est utilisée pour lire les données de l'interface téléinfo.\nIl n'est donc plus possible d'avoir la console par défaut sur ces broches" 
	sudo cp /boot/cmdline.txt /boot/cmdline_backup$heuredebut.txt -n
	sudo sed 's\console=ttyAMA0,115200 kgdboc=ttyAMA0,115200 \ \' /boot/cmdline_backup$heuredebut.txt >/boot/cmdline.txt
	sudo cp /etc/inittab /etc/inittab_backup$heuredebut -n
	sudo sed 's\T0:23:respawn:/sbin/getty -L ttyAMA0 115200 vt100\#T0:23:respawn:/sbin/getty -L ttyAMA0 115200 vt100\' /etc/inittab_backup$heuredebut > /etc/inittab
}

onewire() {
	titre "la préparation de la Raspberry Pi pour l'utilisation du OneWire" "????????????????????????????? NON COMPATIBLE AVEC OWFS ?????????????????????????????????????????????????"
        echo "Il y a pas mal d'explication sur le 1-wire et le PI ici:\nhttp://webshed.org/wiki/RaspberryPI_DS1820\nhttp://www.raspberrypi.org/forums/viewtopic.php?p=477052#p477052"
	sudo modprobe wire
	sudo modprobe w1-gpio
	sudo modprobe w1-therm
	echo "Le driver 1-wire n'est pas chargé par default lors du démarrage du PI" 
	echo "Il faut donc modifier le fichier /etc/modules"
	#/boot/cmdline.txt and add config parameter bcm2708.w1_gpio_pin=<GPIO_pin_number> (for example bcm2708.w1_gpio_pin=25).
	sudo echo wire>>/etc/modules
	sudo echo w1-gpio >>/etc/modules
	sudo echo w1-therm >>/etc/modules
        echo "Ce qui donne le fichier /etc/modules ci-dessous"
	cat </etc/modules
	titre "Création de l'utilitaire showtemp.sh" "La commande showtemp.sh permet d'avoir l'ID des composants présents sur le bus\nListe des identifiants des thermomètres détectés"
	sudo cat /sys/bus/w1/devices/w1_bus_master1/w1_master_slaves
	echo "cat `find /sys -name w1_slave`" >showtemp.sh
	sudo chmod ugo=+rwx showtemp.sh
	titre "Lecture de la température en 1/100 de °C" "Vous devriez voir tous vos thermomètres\n"
	./showtemp.sh
}

main() {
	titre "Installation des utilitaires Teleinfo et OneWire" "$heuredebut\n\n" 
	systeme
	teleinfo
	uart
	onewire
	titre "La Raspberry Pi doit être redémarrée afin que toutes les modifications soient chargées automatiquement" "Tapez\nsudo reboot\nau prompt ci-dessous"
}

# lancement du script principal
main
