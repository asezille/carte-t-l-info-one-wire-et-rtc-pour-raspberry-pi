#!/usr/bin/python
# -*- coding: utf-8 -*-

#----------------------------------------------------------------------------
#  Get RaspberryPi serial number to identify the node
#----------------------------------------------------------------------------
def _get_value(field_name, lenght):
  # Extract serial from cpuinfo file
  try:
    f = open('/proc/cpuinfo','r')
    result = "0000000000000000"
    for line in f:
      name, sep, value = line.rpartition(':')
      if name.startswith(field_name):
        result = value[-lenght-1:-1]
    f.close()
  except:
    result = "0000000000000000"
  return result
  
def getserial():
  # Extract serial from cpuinfo file
  return _get_value('Serial', 16 )
 
def getpcbversion():
  # Extract pcb version from cpuinfo file
  return _get_value('Revision', 4 )

def getprocessor():
  return _get_value('processor','0'.lenght)

def getmodelname():
  return _get_value('model name',32)

def getfeatures():
  return _get_value('Features','swp half thumb fastmult vfp edsp java tls '.lenght)

def getcpuimplementer():
  return _get_value('CPU implementer','0x41'.lenght)

def getcpuarchitecture():
  return _get_value('CPU architecture','7'.lenght)

def getcpuvariant():
  return _get_value('CPU variant','0x0'.lenght)

def getcpupart():
  return _get_value('CPU part', '0xb76'.lenght)

def getcpurevision():
  return _get_value('CPU revision','7'.lenght)

def gethardware():
  return _get_value('Hardware','BCM2708'.lenght)

def getrevision():
  table_info_revision = { 
    #Revision 	Release Date 	Model 	PCB Revision 	Memory 	Notes 
    'Beta' : {'Q1 2012', 'B', '(Beta) ?', '256MB', 'Beta Board'},
    '0002' : {'Q1 2012', 'B', '1.0', '256MB'},
    '0003' : {'Q3 2012', 'B', '(ECN0001)' ,	'1.0' ,	'256MB' ,	'Fuses mod and D14 removed'},
    '0004' : {'Q3 2012', 'B', '2.0', 	'256MB', 	'(Mfg by Sony)'},
    '0005' : {'Q4 2012', 'B', '2.0', 	'256MB', 	'(Mfg by Qisda)'},
    '0006' : {'Q4 2012', 'B', '2.0', 	'256MB', 	'(Mfg by Egoman)'},
    '0007' : {'Q1 2013', 'A', '2.0', 	'256MB', 	'(Mfg by Egoman)'},
    '0008' : {'Q1 2013', 'A', '2.0', 	'256MB', 	'(Mfg by Sony)'},
    '0009' : {'Q1 2013', 'A', '2.0', 	'256MB', 	'(Mfg by Qisda)'},
    '000d' : {'Q4 2012', 'B', '2.0', 	'512MB', 	'(Mfg by Egoman)'},
    '000e' : {'Q4 2012', 'B', '2.0', 	'512MB', 	'(Mfg by Sony)'},
    '000f' : {'Q4 2012', 'B', '2.0', 	'512MB', 	'(Mfg by Qisda)'}
  }
  rev= _get_value('Revision',4)
  return rev, table_info_revision[rev]

def _list_fields():
  try:
    f = open('/proc/cpuinfo','r')
    for line in f:
      name, sep, value = line.rpartition(':')
      print "field name='",name,"' sep='",sep,"' value='",value[:-1],"'"
    f.close()
  except:
    pass

def main():
  print "Raspberry Pi specifics data found in /proc/cpuinfo"
  print "Serial number:", getserial()
  print "PCB revision:", getpcbversion()
  print "Revision:", getrevision()
  _list_fields()

if __name__ == "__main__":
    main()

