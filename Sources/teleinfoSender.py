#!/usr/bin/python
# -*- coding: utf-8 -*-

""" Read one teleinfo frame and output the frame in CSV format on stdout """

import serial
import time
import traceback

import sys
import argparse
 
# ----------------------------------------------------------------------------
# Teleinfo core
# ----------------------------------------------------------------------------
class Teleinfo:
  """ Fetch teleinformation datas and call user callback each time all data are collected  """
  stx='\x02' # begin of frame anything prior is to be ignored
  etx='\x03' # end of frame, normally one for each stx
  eot='\x04' # end of transmission from ERDF power meter, abnormal termination decided by the power meter
  #----------------------------------------------------------------------------
  #  Temporary storage of succeeding frames 
  #----------------------------------------------------------------------------
  lastReadLine="" # empty at first but may contain other things later
  lastReadSTXTimestamp=time.time() # time stamp for last read line
  
  def __init__(self, device, outputfile):
    """  Prepare an instance for receiving data from a device file and sending to an Emoncms
    @param device : teleinfo device path
   """
    self._device = device
    self._outputname=outputfile
    self._ser = None
 
  def __repr__(self):
    """ Provide a textual representation of Teleinfo instance """
    return '<teleinfo device="%s" \ncurFrame="%s"\nprevFrame="%s">' % (self._device, self.curFrame, self.prevFrame)

  def open(self):
    """ open teleinfo device """
    try:
      self._ser = serial.Serial(self._device, 1200, bytesize=7, parity = 'E', stopbits=1)
    except:
      raise Exception("Error opening Teleinfo device '%s' : %s" % (self._device, traceback.format_exc()))
 
  def close(self):
    """ close telinfo device """
    if (self._ser != None)  and (self._ser.isOpen()):
      self._ser.close()

  def terminate(self):
    self.close()
    sys.exit(0)
  
  def _is_valid(self, fieldname, fieldvalue, checksum):
    """ Check if a teleinfo line is valid
    @param fieldname : the field name
    @param fieldvalue : the field value
    @param checksum : the line checksum
    """
    cur_checksum = 0x20 # for intermediate space which is accounted in the checksum
    for cks in fieldname:
      cur_checksum = cur_checksum + ord(cks)
    for cks in fieldvalue:
      cur_checksum = cur_checksum + ord(cks)
    return chr(( cur_checksum & int("111111", 2) ) + 0x20) == checksum
  
  def _analyseLigne(self, line):
    """ checks that the line is containing usefull datas with a valid checksum, sanitise it, split it and return a pair of field name, value """
    resp_splittee=line.replace('\n','').replace('\l','').replace(Teleinfo.etx,'').replace(Teleinfo.stx,'').replace(Teleinfo.eot,'').split()
    # the checksum can be ' ' or any char > ' '
    if len(resp_splittee) == 2: # The checksum char is ' '
      name, value = resp_splittee
      checksum = ' '
    else:  # the checksum char is not a space
      name, value, checksum = resp_splittee
    if self._is_valid(name, value, checksum): # sanitise the input for better readability
      name=name.replace('.','')
      value=value.replace('.','')
      return { name : value }
    else: #This frame is corrupted, we need to wait until the next one
      print "Trame corrompue ! '%s'" % line 
      return None
  
  def read(self):
    """ Récupère les données provenant du port série.
    Si la transmission est corrompue, attente de la trame suivante ce qui peut être long
    @return frame : ligne de texte sous la forme "name:value,"
    où:
      name est le nom légèrement modifié de chaque paramètre teleinfo
      et value est la valeur reçue
    """
    #Get the begin of the frame, marked by Teleinfo.stx
    is_ok = False
    while not is_ok:
      try:
        frame={}
        resp = Teleinfo.lastReadLine
        heureEcoute=Teleinfo.lastReadSTXTimestamp
        while Teleinfo.stx not in resp: 
          resp = self._ser.readline() # catch the begining of the frame, ignoring anything send previously
          heureEcoute=time.time() # A new frame starts without prior stx read
        is_ok = True # so far we have what we need
        resp="" # flush anything read, however we have the right time stamp
        while (is_ok) and (Teleinfo.etx not in resp) and (Teleinfo.eot not in resp):
          resp = self._ser.readline() # we read a single line of text
          if (Teleinfo.eot in resp): # prematured end of frame, nothing to analyse, but error in sight
            print "** Trame interrompue par le compteur ERDF! '%s'" % frame
            is_ok=False
          else:
            resp_analysed=self._analyseLigne(resp)
            if (resp_analysed is None): #This frame is corrupted, we need to wait until the next one
              is_ok=False
            else:
              frame.update(resp_analysed)
      except ValueError:
        #Badly formatted frame
        #This frame is corrupted, we need to wait until the next one
        print "** Trame corrompue, mauvais checksum !"
        is_ok=False
    frame.update({ "timestamp" : heureEcoute })
    # get ready for the new frame's start
    Teleinfo.lastReadLine=resp
    Teleinfo.lastReadSTXTimestamp=time.time()
    # print "Dernière trame reçue '%s'" % str(frame)
    return frame
 
  
  def run(self, nb_secondes):
    """ Main function 
      @param nb_secondes:  number of seconds to scan the device and send to the emoncms server 
    """
    # Open Teleinfo device
    self.open()
    self._heureDebut=time.time()
    self._heureFin=self._heureDebut+nb_secondes
    oldframeJson={}
    while time.time()<self._heureFin:
        frameJson = self.read() # Lecture d'une trame valide
        for k in frameJson:
          if ((k not in oldframeJson) or (oldframeJson[k]!=frameJson[k])):
            oldframeJson[k]=frameJson[k]
            if (k!='timestamp'):
              nomf=self._outputname+"_"+str(k)+".log"
              with open(nomf, 'a') as f:
                f.write(str(frameJson['timestamp'])+","+str(frameJson[k])+"\n")
    # This is the End!
    self.terminate()
 
#------------------------------------------------------------------------------
# MAIN
#------------------------------------------------------------------------------
def main():
  parser = argparse.ArgumentParser(description='Collecte des données de la liaison Teleinfo.')
  parser.add_argument('-d', '--duree',help='%(prog)s a besoin de la durée pendant laquelle il va stocker les données de la liaison téléinfo, 1 an par défaut', type=int, default=3600*24*365)
  parser.add_argument('-t', '--teleinfo',help='%(prog)s a besoin du nom de fichier de la liaison téléinfo. Note il faut que ce soit un fichier tty (/dev/ttyAMA0 par défaut)',default='/dev/ttyAMA0')
  parser.add_argument('-l', '--log',help='%(prog)s a besoin du nom de la racine des fichiers de stockage des données horodatées de la liaison téléinfo (/tmp/teleinfo par défaut)',default='/tmp/teleinfo')
  ar=parser.parse_args()
  outputfile=ar.log
  duree=ar.duree
  fichierTeleinfo=ar.teleinfo
  print 'Prêt a envoyer les données dans %s_nom du champ teleinfo.log' % outputfile
  print 'Les données seront lues sur %s' % fichierTeleinfo
  print 'Les données seront lues pendant %s secondes' % duree
  teleinfo = Teleinfo(fichierTeleinfo, outputfile)
  teleinfo.run(duree)


if __name__ == "__main__":
  main()
